import CalculatorMeal from "../../features/calculator/index.jsx";
import Typography from "antd/es/typography";
import {useLocation} from "react-router-dom";

const {Title} = Typography;
export const CalculatorPage = () => {
    const location = useLocation()

    const demoView = location.pathname === '/calculator-demo'

    return (
        <div style={{display: 'flex', flexDirection: 'column', gap: '24px'}}>
            <Title level={2}
                   style={{margin: 0}}>{`Калькулятор приема пищи ${demoView ? '(ограниченный для демо)' : ''}`}</Title>
            <CalculatorMeal/>
        </div>
    )

}