import {Button} from "antd";

export const CustomButton = ({ text, ...props }) => {

    return (
        <Button {...props}>
            {text}
        </Button>
    )
}