import {createBrowserRouter} from "react-router-dom";
import {BaseLayout} from "./layouts/BaseLayout.jsx";
import {CalculatorPage} from "../pages/calculator/page.jsx";
import DirectoryProductsView from "../features/directories/DirectoryProductsView/view.jsx";
import DirectoryDishesView from "../features/directories/DirectoryDishesView/view.jsx";


export const router = createBrowserRouter([{
    path: '/',
    element: <BaseLayout role={'user'}/>,
    children: [
        {path: 'calculator', element: <CalculatorPage/>},
        {
            path: 'directories',
            children: [
                {path: 'products', element: <DirectoryProductsView/>},
                {path: 'dishes', element: <DirectoryDishesView/>}
            ]
        },
    ]
},
]);