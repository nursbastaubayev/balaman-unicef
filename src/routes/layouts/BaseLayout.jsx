import {Layout, Menu, theme} from 'antd';
import {Link, Outlet, useNavigate} from "react-router-dom";
import LocalDiningIcon from '@mui/icons-material/LocalDining';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import CalculateIcon from '@mui/icons-material/Calculate';
import logo from '../../assets/logo_single.svg'
import './layoutCss.css';
import {useState} from "react";

const {Content, Sider} = Layout;

function getItem(label, key, icon, children) {
    return {
        key, icon, children, label,
    };
}

const items = {
    'user': [
        getItem('Калькулятор', '/calculator', <CalculateIcon/>),
        getItem('Справочники', '/directories', <AutoStoriesIcon/>, [
            getItem('Продукты', '/directories/products', <LocalDiningIcon/>),
            getItem('Блюда', '/directories/dishes', <FastfoodIcon/>),]),],
}


export const BaseLayout = ({role}) => {
    const navigate = useNavigate()
    const [collapsed, setCollapsed] = useState(false);
    const {
        token: {colorBgContainer, borderRadiusLG},
    } = theme.useToken();


    return (<Layout
        style={{
            height: '100vh',
        }}
    >
        <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}
               style={{
                   background: colorBgContainer,
                   borderRadius: borderRadiusLG,
                   height: '100vh',
                   overflow: 'auto',
                   padding: '32px 0',
                   boxShadow: '0px 2px 2px rgba(0, 0, 0, 0.15)',
                   display: 'flex',
                   flexDirection: 'column',
               }}
        >
            <div style={{
                display: 'flex', width: '100%', alignItems: 'center', justifyContent: 'center', marginBottom: '24px'
            }}>
                <Link to={`/${role}`}>
                    <img src={logo} style={{width: 40, height: 40, cursor: 'pointer'}} alt={'...logo'}/>
                </Link>
            </div>

                <Menu theme={'light'} defaultSelectedKeys={['1']} mode="inline" items={items[role]}
                      onClick={(item) => navigate(item.key)}
                    // Set font size here
                />

        </Sider>
        <Layout style={{flex: 1, background: '#F6F8FB',}}>
            <Content
                style={{
                    margin: '24px 16px', borderRadius: borderRadiusLG, overflowY: 'auto', // Add scroll only for Y-axis
                }}
            >
                <Outlet/>
            </Content>
        </Layout>
    </Layout>);
};
