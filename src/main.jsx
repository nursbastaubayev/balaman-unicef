import ReactDOM from 'react-dom/client'
import './index.css'
import {Provider} from "react-redux";
import {store} from "./store/store.jsx";
import {RouterProvider} from "react-router-dom";
import {router} from "./routes/index.jsx";
import {persistStore} from "redux-persist";
import {PersistGate} from "redux-persist/integration/react";

const persistor = persistStore(store)

ReactDOM.createRoot(document.getElementById('root')).render(
    <Provider store={store}>
        <PersistGate persistor={persistor}>
                <RouterProvider router={router}/>
        </PersistGate>
    </Provider>
)
