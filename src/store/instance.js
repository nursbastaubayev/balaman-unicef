import axios from 'axios'

export const instance = axios.create({
    baseURL: 'https://api-balaman2.cloudoc.kz/api',
    withCredentials: false,
});
