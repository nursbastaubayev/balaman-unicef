import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {instance} from "../../instance.js";

export const calculateNutrients = createAsyncThunk('calculator/calculateNutrients', async data => {
  try {
    const response = await instance.post('/calculate-total-nutrients', {
      products: data
    });
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})

export const getAllFactors = createAsyncThunk('calculator/getAllFactors', async data => {
  try {
    const response = await instance.get('/factors');
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})

export const getNutrientsDetailByProduct = createAsyncThunk('calculator/getNutrientsDetailByProduct', async data => {
  try {
    const response = await instance.post('/nutrient-details', data);
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})


export const calculateMeal = createAsyncThunk('calculator/calculateMeal', async data => {
  try {
    const response = await instance.post('/dishes-calculate-total-nutrients', data);
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})


const initialState = {
  statuses: {
    isPending: false, isError: false, isFulfilled: false
  }, factors: []
}

export const calculatorSlice = createSlice({
  name: 'calculator', initialState, reducers: {}, extraReducers: builder => {
    builder.addCase(calculateNutrients.fulfilled, (state) => {
      state.statuses.isPending = false;
      state.statuses.isFulfilled = true;
      state.statuses.isError = false;
    });
    builder.addCase(calculateNutrients.pending, (state) => {
      state.statuses.isPending = true;
      state.statuses.isFulfilled = false;
      state.statuses.isError = false;
    });
    builder.addCase(calculateNutrients.rejected, (state, {payload}) => {
      state.statuses.isPending = false;
      state.statuses.isFulfilled = false;
      state.statuses.isError = true;
    });
    builder.addCase(getAllFactors.fulfilled, (state, {payload}) => {
      state.factors = payload;
    });

  }

})
//
// // Action creators are generated for each case reducer function
export const {} = calculatorSlice.actions

export default calculatorSlice.reducer

