import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {instance} from "../../instance.js";

export const getDishesAll = createAsyncThunk('dishes/getDishesAll', async data => {
    try {
        const response = await instance.get('/dishes', {
            params: data
        });
        return response.data;
    } catch (error) {
        throw new Error('Failed to fetch user data');
    }
})

export const getDishDetailsByID = createAsyncThunk('dishes/getDishDetailsByID', async data => {
    try {
        const response = await instance.get('/dishes', {
            params: {
                dish_id: data
            }
        });
        return response.data;
    } catch (error) {
        throw new Error('Failed to fetch user data');
    }
})

export const getDishesByNameSearch = createAsyncThunk('dishes/getDishesByNameSearch', async data => {
    try {
        const response = await instance.get('/dishes', {
            params: {
                search: data,
                per_page: 50,
            }
        });
        return response.data;
    } catch (error) {
        throw new Error('Failed to fetch user data');
    }
})


export const createDish = createAsyncThunk('dishes/createDish', async data => {
    try {
        const response = await instance.post('/dishes', {
            ...data,

            health_factor: 5,
            image_url: "http://example.com/images/salad.jpg",
        });
        return response.data;
    } catch (error) {
        throw new Error('Failed to fetch user data');
    }
})


const initialState = {
    statuses: {
        isPending: false, isError: false, isFulfilled: false
    }, list: {
        current_page: 1,
        items_per_page: 10,
        total_items: 0,
        total_pages: 0,
        data: []
    }
}

export const dishesSlice = createSlice({
    name: 'dishes', initialState, reducers: {}, extraReducers: builder => {
        builder.addCase(getDishesAll.fulfilled, (state, {payload}) => {
            state.statuses.isPending = false;
            state.statuses.isFulfilled = true;
            state.statuses.isError = false;
            state.list = payload;
        });
        builder.addCase(getDishesAll.pending, (state) => {
            state.statuses.isPending = true;
            state.statuses.isFulfilled = false;
            state.statuses.isError = false;
        });
        builder.addCase(getDishesAll.rejected, (state, {payload}) => {
            state.statuses.isPending = false;
            state.statuses.isFulfilled = false;
            state.statuses.isError = true;
        });
        builder.addCase(getDishesByNameSearch.fulfilled, (state, {payload}) => {
            state.statuses.isPending = false;
            state.statuses.isFulfilled = true;
            state.statuses.isError = false;
            state.list = payload;
        });
        builder.addCase(getDishesByNameSearch.pending, (state) => {
            state.statuses.isPending = true;
            state.statuses.isFulfilled = false;
            state.statuses.isError = false;
        });
        builder.addCase(getDishesByNameSearch.rejected, (state, {payload}) => {
            state.statuses.isPending = false;
            state.statuses.isFulfilled = false;
            state.statuses.isError = true;
        });
    }

})
//
// // Action creators are generated for each case reducer function
export const {} = dishesSlice.actions

export default dishesSlice.reducer

