import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {instance} from "../../instance.js";

export const getProductsAll = createAsyncThunk('products/getProductsAll', async data => {
  try {
    const response = await instance.get('/products', {
      params: data
    });
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})

export const getProductsCategories = createAsyncThunk('products/getProductsCategories', async data => {
  try {
    const response = await instance.get('/product-categories');
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})

export const getProductDetailsByID = createAsyncThunk('products/getProductDetailsByID', async data => {
  try {
    const response = await instance.get('/products', {
      params: {
        product_id: data
      }
    });
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})

export const getProductsByNameSearch = createAsyncThunk('products/getProductsByNameSearch', async data => {
  try {
    const response = await instance.get('/products', {
      params: {
        search: data,
        per_page: 50,
      }
    });
    return response.data;
  } catch (error) {
    throw new Error('Failed to fetch user data');
  }
})


const initialState = {
  statuses: {
    isPending: false, isError: false, isFulfilled: false
  }, list: {
    current_page: 1,
    items_per_page: 10,
    total_items: 0,
    total_pages: 0,
    data: []
  }, categories:[]
}

export const productsSlice = createSlice({
  name: 'products', initialState, reducers: {}, extraReducers: builder => {
    builder.addCase(getProductsAll.fulfilled, (state, {payload}) => {
      state.statuses.isPending = false;
      state.statuses.isFulfilled = true;
      state.statuses.isError = false;
      state.list = payload;
    });
    builder.addCase(getProductsAll.pending, (state) => {
      state.statuses.isPending = true;
      state.statuses.isFulfilled = false;
      state.statuses.isError = false;
    });
    builder.addCase(getProductsAll.rejected, (state, {payload}) => {
      state.statuses.isPending = false;
      state.statuses.isFulfilled = false;
      state.statuses.isError = true;
    });
    builder.addCase(getProductsCategories.fulfilled, (state, {payload}) => {
      state.categories = payload;
    });
    builder.addCase(getProductsByNameSearch.fulfilled, (state, {payload}) => {
      state.statuses.isPending = false;
      state.statuses.isFulfilled = true;
      state.statuses.isError = false;
      state.list = payload;
    });
    builder.addCase(getProductsByNameSearch.pending, (state) => {
      state.statuses.isPending = true;
      state.statuses.isFulfilled = false;
      state.statuses.isError = false;
    });
    builder.addCase(getProductsByNameSearch.rejected, (state, {payload}) => {
      state.statuses.isPending = false;
      state.statuses.isFulfilled = false;
      state.statuses.isError = true;
    });
  }

})
//
// // Action creators are generated for each case reducer function
export const {} = productsSlice.actions

export default productsSlice.reducer

