import {combineReducers, configureStore} from '@reduxjs/toolkit'
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import productsReducer from "./slices/directories/productsSlice.js";
import dishesReducer from "./slices/directories/dishesSlice.js";
import calculatorReducer from "./slices/main/calculatorSlice.js";


const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['system'],
}


export const rootReducers = combineReducers({
    products: productsReducer,
    dishes: dishesReducer,
    calculator: calculatorReducer,
})
const persistedReducer = persistReducer(persistConfig, rootReducers)
export const store = configureStore({
    reducer: persistedReducer,
})
