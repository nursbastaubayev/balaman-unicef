import {useDispatch, useSelector} from "react-redux";
import {
    getProductDetailsByID,
    getProductsAll,
    getProductsCategories
} from "../../../store/slices/directories/productsSlice.js";
import {useEffect, useState} from "react";
import Drawer from "@mui/material/Drawer";
import {CircularProgress, InputBase} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import {CustomNoRowsOverlay} from "../../../components/emptyComponent/emptyComponent.jsx";
import {Table} from "antd";
import {CookingMethodIcons} from "../../calculator/components/utils.jsx";
import {DoubleRightOutlined, FilterOutlined, SortAscendingOutlined} from "@ant-design/icons";
import Typography from "antd/es/typography";
import {consumeStandard} from "../../calculator/components/constants.jsx";
import {ResultsSection} from "../../calculator/components/resultsSection.jsx";
import {calculateMeal} from "../../../store/slices/main/calculatorSlice.js";

const {Text, Title} = Typography;
// ----------------------------------------------------------------------

export default function DirectoryProductsView() {
    const dispatch = useDispatch()
    const products = useSelector((state) => state.products)
    const [openDrawer, setOpenDrawer] = useState(false);
    const [detailsLoading, setDetailsLoading] = useState(false)
    const [details, setDetails] = useState(null)
    const [search, setSearch] = useState('')
    const [category, setCategory] = useState(null)

    const [result, setResult] = useState(null)
    const [ageCategory, setAgeCategory] = useState('0-2'); // Default to '0-2' age category
    const standards = consumeStandard[ageCategory];

    const productsColumns = [{
        title: 'ID', dataIndex: 'product_id', key: 'product_id', render: (text, record, index) => <span>{text}</span>,
    }, {
        title: 'Продукт', dataIndex: 'name', key: 'name', render: (text, record, index) => <span>{text}</span>,
    }, {
        title: 'Категория',
        dataIndex: 'product_category',
        key: 'product_category',
        render: (text, record, index) => <span>{text.name}</span>,
    }, {
        title: 'ID Категории',
        dataIndex: 'product_category',
        key: 'product_category_id',
        render: (text, record, index) => <span>{text.product_category_id}</span>,
    }, {
        title: 'Факторы',
        dataIndex: 'factor_ids',
        key: 'factor_ids',
        render: (ids) => CookingMethodIcons({methods: ids}),
    }, {
        title: 'Вес', key: 'weight', dataIndex: 'weight', render: (text) => <span>{text} г</span>,
    }, {
        title: '',
        key: 'action',
        render: (_, record, index) => <div
            style={{display: 'flex', gap: '6px', justifyContent: 'end', alignItems: 'center', cursor: 'pointer'}}
            onClick={() => handleDrawerOpen(record.product_id)}>
            <Text>Подробнее</Text>
            <DoubleRightOutlined style={{fontSize: '16px'}}
            />
        </div>
    },];


    const handleDrawerClose = () => {
        setOpenDrawer(false);
        setDetails(null)
    }

    const handleDrawerOpen = (id) => {
        setOpenDrawer(true);
        setDetailsLoading(true)
        dispatch(calculateMeal({
            products: [{
                factor_ids: [],
                product_id: id,
                weight: 100
            }]
        })).then((res) => setResult(res.payload))
        dispatch(getProductDetailsByID(id))
            .then((res) => setDetails(res.payload))
            .then(() => setDetailsLoading(false))
    }
    const getProducts = (currentPage = 1, perPage = 10) => {
        dispatch(getProductsAll({
            page: currentPage, per_page: perPage, search: search, product_category_id: category
        }))
    }

    useEffect(() => {
        dispatch(getProductsAll({
            per_page: products.list.items_per_page,
            page: products.list.current_page - 1,
            search: search,
            product_category_id: category
        }))
    }, [search, category])

    useEffect(() => {
        dispatch(getProductsCategories())
        products.list.data.length === 0 && getProducts()
    }, [])

    const [timerId, setTimerId] = useState(null);

    const handleSearch = (value) => {
        // Clear the previous timer to ensure only the last call is executed
        if (timerId) {
            clearTimeout(timerId);
        }

        // Set a new timer
        const newTimerId = setTimeout(() => {
            setSearch(value);
        }, 2000); // 2 seconds delay

        setTimerId(newTimerId);
    };


    return (<>
        <Title level={3}>Справочники: Продукты</Title>
        <div style={{
            display: 'flex', flexDirection: 'column', gap: '12px',

        }}>

            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <div style={{
                    borderRadius: '8px', background: '#FFF', width: '300px', boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)'
                }}>
                    <InputBase
                        size={'small'}
                        placeholder="Поиск"
                        sx={{padding: '4px 8px'}}
                        onChange={(e) => handleSearch(e.target.value)}
                        startAdornment={<SearchIcon/>}

                    />
                </div>

                <div style={{display: 'flex', gap: '12px'}}>
                    <div style={{
                        borderRadius: '8px',
                        background: '#FFF',
                        boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '0px 12px',
                        gap: '6px'
                    }}>
                        <SortAscendingOutlined/>
                        Сортировка
                    </div>
                    <div style={{
                        borderRadius: '8px',
                        background: '#FFF',
                        boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '0px 12px',
                        gap: '6px'
                    }}>
                        <FilterOutlined/>
                        Фильтр
                    </div>
                </div>
            </div>


            <div style={{
                boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                borderRadius: '10px',
                display: 'flex',
                flexDirection: 'column',
                gap: '24px',
                padding: '24px',
                background: '#FFF'
            }}>
                <Table columns={productsColumns} dataSource={products.list.data} pagination={{
                    position: ['none', 'bottomCenter'],
                }} size={'small'}
                       locale={{
                           emptyText: <CustomNoRowsOverlay text={'Добавьте блюда для расчета'}/>,
                       }}
                       style={{minHeight: '250px'}}
                />
            </div>

        </div>

        <Drawer
            anchor="right"
            open={openDrawer}
            onClose={handleDrawerClose}
            slotProps={{
                backdrop: {invisible: true},
            }}
        >
            {detailsLoading && <CircularProgress sx={{margin: 'auto auto'}}/>}
            {details &&
                <div style={{flexGrow: 1, textAlign: 'center'}}>
                    <Title level={5} style={{fontWeight: 'bold', textDecoration: 'underline'}}>{details.name}</Title>
                </div>

            }
            {result && <div style={{padding: '12px'}}>
                <ResultsSection result={result} ageCategory={ageCategory} setAgeCategory={setAgeCategory}
                                standards={standards}/>
            </div>}

        </Drawer>
    </>);


}
