import Typography from "antd/es/typography";
import {CustomNoRowsOverlay} from "../../../components/emptyComponent/emptyComponent.jsx";
import {Table} from "antd";
import {CookingMethodIcons} from "../../calculator/components/utils.jsx";
import TextField from "@mui/material/TextField";

const {Text, Title} = Typography;

export const DishDetails = ({details}) => {

    const ProductsTableColumns = [{
        title: 'Продукт',
        dataIndex: 'name',
        key: 'name',
        render: (text, record, index) => <span>{`${index + 1}. ${text}`}</span>,
    },
        {
            title: 'Факторы',
            dataIndex: 'pivot',
            key: 'factor_ids',
            render: (ids) => CookingMethodIcons({methods: ids.factor_ids}),
        },
        {
            title: 'Вес', key: 'weight', dataIndex: 'pivot', render: (text) => <span>{text.weight} г</span>,
        }];

    return (
        <>
            <div style={{display: 'flex', flexDirection: 'column', padding: '12px',}}>
                <Title level={5} style={{marginBottom: '12px'}}>{'Состав продуктов:'}</Title>
                <div style={{
                    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                    borderRadius: '10px',
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '24px',

                }}>
                    <Table columns={ProductsTableColumns} dataSource={details.products} pagination={{
                        position: ['none', 'none'],
                    }}
                           size={'small'}
                           locale={{
                               emptyText: <CustomNoRowsOverlay text={'Добавьте продукты для расчета'}/>,
                           }}
                    /></div>
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                marginBottom: '16px',
                padding: '12px'
            }}>
                <Title level={5} style={{marginBottom: '12px'}}>Описание блюда</Title>
                <TextField
                    InputProps={{
                        readOnly: true,
                    }}
                    size={'small'}
                    placeholder={'Введите описание блюда'}
                    fullWidth
                    multiline
                    rows={4}
                    variant="standard" value={details.recipe_description}
                />
            </div>
        </>

    )
}
