import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import Drawer from "@mui/material/Drawer";
import {CircularProgress, InputBase,} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';

import {DishDetails} from "./details.jsx";

import {CustomNoRowsOverlay} from "../../../components/emptyComponent/emptyComponent.jsx";
import {getDishDetailsByID, getDishesAll} from "../../../store/slices/directories/dishesSlice.js";
import {DoubleRightOutlined, FilterOutlined, SortAscendingOutlined} from "@ant-design/icons";
import {Table} from "antd";
import Typography from "antd/es/typography";
import {consumeStandard} from "../../calculator/components/constants.jsx";
import {ResultsSection} from "../../calculator/components/resultsSection.jsx";
import {calculateMeal} from "../../../store/slices/main/calculatorSlice.js";


const {Text, Title} = Typography;
// ----------------------------------------------------------------------

export default function DirectoryDishesView() {

    const dispatch = useDispatch()
    const dishes = useSelector((state) => state.dishes)
    const [openDetailsDrawer, setOpenDetailsDrawer] = useState(false);
    const [detailsLoading, setDetailsLoading] = useState(false)
    const [details, setDetails] = useState(null)
    const [search, setSearch] = useState('')
    const [result, setResult] = useState(null)
    const [ageCategory, setAgeCategory] = useState('0-2'); // Default to '0-2' age category
    const standards = consumeStandard[ageCategory];
    const handleDetailsDrawerClose = () => {
        setOpenDetailsDrawer(false);
        setDetails(null)
    }
    const handleDetailsDrawerOpen = (id) => {
        setOpenDetailsDrawer(true);
        setDetailsLoading(true)
        dispatch(calculateMeal({dishes: [id]})).then((res) => setResult(res.payload))
        dispatch(getDishDetailsByID(id))
            .then((res) => setDetails(res.payload))
            .then(() => setDetailsLoading(false))
    }
    const getDishes = (currentPage = 1, perPage = 10) => {
        dispatch(getDishesAll({
            page: currentPage, per_page: perPage, search: search
        }))
    }
    const handlePagination = (pagination) => {
        getDishes(pagination.page + 1, pagination.pageSize)
    }

    useEffect(() => {
        dispatch(getDishesAll({
            per_page: dishes.list.items_per_page, page: dishes.list.current_page - 1, search: search
        }))
    }, [search])

    useEffect(() => {
        dishes.list.data.length === 0 && getDishes()
    }, [])

    const [timerId, setTimerId] = useState(null);

    const handleSearch = (value) => {
        // Clear the previous timer to ensure only the last call is executed
        if (timerId) {
            clearTimeout(timerId);
        }

        // Set a new timer
        const newTimerId = setTimeout(() => {
            setSearch(value);
        }, 2000); // 2 seconds delay

        setTimerId(newTimerId);
    };


    const dishesColumns = [{
        title: 'ID', dataIndex: 'dish_id', key: 'dish_id', render: (text, record, index) => <span>{text}</span>,
    }, {
        title: 'Блюда', dataIndex: 'name', key: 'name', render: (text, record, index) => <span>{text}</span>,
    }, {
        title: 'Категория',
        dataIndex: 'dish_category',
        key: 'dish_category',
        render: (text, record, index) => <span>{text.dish_category_id}</span>,
    }, {
        title: 'ID Категории',
        dataIndex: 'dish_category',
        key: 'dish_category_id',
        render: (text, record, index) => <span>{text.dish_category_id}</span>,
    }, {
        title: '', key: 'action', render: (_, record, index) => <div
            style={{display: 'flex', gap: '6px', justifyContent: 'end', alignItems: 'center', cursor: 'pointer'}}
            onClick={() => handleDetailsDrawerOpen(record.dish_id)}>
            <Text>Подробнее</Text>
            <DoubleRightOutlined style={{fontSize: '16px'}}
            />
        </div>
    },];

    return (<>
        <Title level={3}>Справочники: Блюда</Title>
        <div style={{
            display: 'flex', flexDirection: 'column', gap: '12px',
        }}>
            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <div style={{
                    borderRadius: '8px', background: '#FFF', width: '300px', boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)'
                }}>
                    <InputBase
                        size={'small'}
                        placeholder="Поиск"
                        sx={{padding: '4px 8px'}}
                        onChange={(e) => handleSearch(e.target.value)}
                        startAdornment={<SearchIcon/>}

                    />
                </div>

                <div style={{display: 'flex', gap: '12px'}}>
                    <div style={{
                        borderRadius: '8px',
                        background: '#FFF',
                        boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '0px 12px',
                        gap: '6px'
                    }}>
                        <SortAscendingOutlined/>
                        Сортировка
                    </div>
                    <div style={{
                        borderRadius: '8px',
                        background: '#FFF',
                        boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '0px 12px',
                        gap: '6px'
                    }}>
                        <FilterOutlined/>
                        Фильтр
                    </div>
                </div>
            </div>


            <div style={{
                boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                borderRadius: '10px',
                display: 'flex',
                flexDirection: 'column',
                gap: '24px',
                padding: '24px',
                background: '#FFF'
            }}>
                <Table columns={dishesColumns} dataSource={dishes.list.data} pagination={{
                    position: ['none', 'bottomCenter'],
                }} size={'small'}
                       locale={{
                           emptyText: <CustomNoRowsOverlay text={'Добавьте блюда для расчета'}/>,
                       }}
                       style={{minHeight: '250px'}}
                />
            </div>

        </div>
        <Drawer
            anchor="right"
            open={openDetailsDrawer}
            onClose={handleDetailsDrawerClose}
            slotProps={{
                backdrop: {invisible: true},
            }}
        >
            {detailsLoading && <CircularProgress sx={{margin: 'auto auto'}}/>}
            {details &&
                <div style={{flexGrow: 1, textAlign: 'center'}}>
                    <Title level={5} style={{fontWeight: 'bold', textDecoration: 'underline'}}>{details.name}</Title>
                </div>

            }
            {result && <div style={{padding: '12px'}}>
                <ResultsSection result={result} ageCategory={ageCategory} setAgeCategory={setAgeCategory}
                                standards={standards}/>
            </div>}

            {details && <DishDetails details={details}/>}
        </Drawer>
    </>);


}
