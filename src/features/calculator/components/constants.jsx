export const consumeStandard = {
  "0-2": {
    total_kilocalories: 1200,
    total_protein: 30,
    total_fat: 65,
    total_carbohydrate: 152,
    //minerals
    calcium: 400, //mg   Кальций
    phosphorus: 400, //mg Фосфор
    magnesium: 60, //mg Магний
    iron: 10, //mg Железо
    zinc: 4, //mg Цинк
    iodine: 90, //mcg  Йод
    copper: 250, //mcg Медь
    potassium: 400, //mg Калий
    sodium: 1, //g Натрий
    water: 0.2, //l Вода
    //vitamins
    vitaminA: 500, //mcg
    vitaminD: 5, //mcg
    vitaminE: 5, //mg
    vitaminK: 10, //mcg
    vitaminC: 50, //mg
    vitaminB1: 0.3, //mg
    vitaminB2: 0.4, //mg
    vitaminB3: 4, //mg
    vitaminB5: 1.8, //mg
    vitaminB6: 0.3, //mg
    vitaminB7: 6, //mcg
    vitaminB9: 80, //mcg
    vitaminB12: 0.7, //mcg
    fiber: 10, //g

  },
  "2-3": {
    total_kilocalories: 1400,
    total_protein: 39,
    total_fat: 65,
    total_carbohydrate: 200,
    //minerals
    calcium: 700, //mg   Кальций
    phosphorus: 400, //mg Фосфор
    magnesium: 80, //mg Магний
    iron: 10, //mg Железо
    zinc: 5, //mg Цинк
    iodine: 90, //mcg  Йод
    copper: 350, //mcg Медь
    potassium: 400, //mg Калий
    sodium: 1, //g Натрий
    water: 0.7, //l Вода
    //vitamins
    vitaminA: 400, //mcg
    vitaminD: 10, //mcg
    vitaminE: 6, //mg
    vitaminK: 30, //mcg
    vitaminC: 30, //mg
    vitaminB1: 0.5, //mg
    vitaminB2: 0.5, //mg
    vitaminB3: 6, //mg
    vitaminB5: 2, //mg
    vitaminB6: 0.5, //mg
    vitaminB7: 8, //mcg
    vitaminB9: 150, //mcg
    vitaminB12: 0.9, //mcg
    fiber: 10, //g
  },
  "4-6": {
    total_kilocalories: 1800,
    total_protein: 50,
    total_fat: 65,
    total_carbohydrate: 266,
    //minerals
    calcium: 800, //mg   Кальций
    phosphorus: 500, //mg Фосфор
    magnesium: 100, //mg Магний
    iron: 10, //mg Железо
    zinc: 5, //mg Цинк
    iodine: 90, //mcg  Йод
    copper: 450, //mcg Медь
    potassium: 600, //mg Калий
    sodium: 1, //g Натрий
    water: 1.1, //l Вода
    //vitamins
    vitaminA: 450, //mcg
    vitaminD: 10, //mcg
    vitaminE: 7, //mg
    vitaminK: 55, //mcg
    vitaminC: 30, //mg
    vitaminB1: 0.6, //mg
    vitaminB2: 0.6, //mg
    vitaminB3: 8, //mg
    vitaminB5: 3, //mg
    vitaminB6: 0.6, //mg
    vitaminB7: 12, //mcg
    vitaminB9: 200, //mcg
    vitaminB12: 1.2, //mcg
    fiber: 25, //g
  },
  "7-10": {
    total_kilocalories: 2100,
    total_protein: 63,
    total_fat: 65,
    total_carbohydrate: 315,
    //minerals
    calcium: 1000, //mg   Кальций
    phosphorus: 800, //mg Фосфор
    magnesium: 200, //mg Магний
    iron: 12, //mg Железо
    zinc: 8, //mg Цинк
    iodine: 120, //mcg  Йод
    copper: 700, //mcg Медь
    potassium: 900, //mg Калий
    sodium: 1.5, //g Натрий
    water: 1.7, //l Вода
    //vitamins
    vitaminA: 600, //mcg
    vitaminD: 10, //mcg
    vitaminE: 11, //mg
    vitaminK: 60, //mcg
    vitaminC: 45, //mg
    vitaminB1: 0.9, //mg
    vitaminB2: 0.9, //mg
    vitaminB3: 12, //mg
    vitaminB5: 4, //mg
    vitaminB6: 1, //mg
    vitaminB7: 20, //mcg
    vitaminB9: 300, //mcg
    vitaminB12: 1.8, //mcg
    fiber: 26, //g
  },
  "11-14": {
    total_kilocalories: 2450,
    total_protein: 73,
    total_fat: 60,
    total_carbohydrate: 370,
    //minerals
    calcium: 1300, //mg   Кальций
    phosphorus: 1200, //mg Фосфор
    magnesium: 300, //mg Магний
    iron: 20, //mg Железо
    zinc: 10, //mg Цинк
    iodine: 150, //mcg  Йод
    copper: 850, //mcg Медь
    potassium: 2000, //mg Калий
    sodium: 1.5, //g Натрий
    water: 2, //l Вода
    //vitamins
    vitaminA: 800, //mcg
    vitaminD: 10, //mcg
    vitaminE: 15, //mg
    vitaminK: 75, //mcg
    vitaminC: 75, //mg
    vitaminB1: 1.2, //mg
    vitaminB2: 1.3, //mg
    vitaminB3: 16, //mg
    vitaminB5: 5, //mg
    vitaminB6: 1.3, //mg
    vitaminB7: 25, //mcg
    vitaminB9: 400, //mcg
    vitaminB12: 2.4, //mcg
    fiber: 31, //g
  },
  "15-18": {
    total_kilocalories: 2700,
    total_protein: 81,
    total_fat: 60,
    total_carbohydrate: 405,
    //minerals
    calcium: 1300, //mg   Кальций
    phosphorus: 1200, //mg Фосфор
    magnesium: 300, //mg Магний
    iron: 20, //mg Железо
    zinc: 10, //mg Цинк
    iodine: 150, //mcg  Йод
    copper: 850, //mcg Медь
    potassium: 2000, //mg Калий
    sodium: 1.5, //g Натрий
    water: 2, //l Вода
    //vitamins
    vitaminA: 800, //mcg
    vitaminD: 10, //mcg
    vitaminE: 15, //mg
    vitaminK: 75, //mcg
    vitaminC: 75, //mg
    vitaminB1: 1.2, //mg
    vitaminB2: 1.3, //mg
    vitaminB3: 16, //mg
    vitaminB5: 5, //mg
    vitaminB6: 1.3, //mg
    vitaminB7: 25, //mcg
    vitaminB9: 400, //mcg
    vitaminB12: 2.4, //mcg
    fiber: 36, //g
  },
}



