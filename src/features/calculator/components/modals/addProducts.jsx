import {Modal, Typography} from 'antd';
import Box from "@mui/material/Box";
import {Autocomplete} from "@mui/lab";
import TextField from "@mui/material/TextField";
import {ListItemIcon} from "@mui/material";
import RestaurantMenuIcon from "@mui/icons-material/RestaurantMenu.js";
import ListItemText from "@mui/material/ListItemText";
import MenuBookIcon from "@mui/icons-material/MenuBook.js";
import {NumericFormat} from "react-number-format";
import InputAdornment from "@mui/material/InputAdornment";
import {CustomButton} from "../../../../ui/button/button.jsx";

const {Text, Title} = Typography;
export const AddProductAndDishModal = ({
                                           open,
                                           onSubmit,
                                           onCancel,
                                           type,
                                           options,
                                           value,
                                           setValue,
                                           factors,
                                           setOptions,
                                           handleFactorChange,
                                           weight,
                                           setWeight,
                                           setInputValue,
                                           productFactor,
                                           setProductFactor
                                       }) => {

    return (<Modal title={<Title level={4}>Добавление {type === 'product' ? 'продукта' : 'блюда'}</Title>} open={open}
                   onCancel={onCancel}
                   footer={[<CustomButton size={'large'} type={'primary'} onClick={onSubmit} text={'Добавить'}
                                          style={{width: '100%'}} disabled={!value}/>]}>
        <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
            <Text style={{color: "#5B5B5B"}}>Выберите {type === 'product' ? 'продукт' : 'блюдо'}</Text>
            <Autocomplete
                size={'small'}
                getOptionKey={(option) => option.product_id}
                filterOptions={(x) => x}
                options={options}
                autoComplete
                value={value}
                getOptionLabel={(option) => typeof option === 'string' ? option : option.name}
                onChange={(event, newValue) => {
                    setOptions(newValue ? [newValue, ...options] : options);
                    setValue(newValue);
                }}
                onInputChange={(event, newInputValue) => {
                    setInputValue(newInputValue);
                }}
                renderInput={(params) => (<TextField placeholder={'Начните вводить название'} {...params} fullWidth/>)}
                renderOption={(props, option) => {
                    // If the condition is based on `open.type` being 'product'
                    if (type === 'product') {
                        return (<Box component="li" {...props}>
                            <ListItemIcon>
                                <RestaurantMenuIcon
                                    color={option.factor_ids?.length > 0 ? 'success' : 'default'}/>
                            </ListItemIcon>
                            <ListItemText primary={option.name}/>
                        </Box>);
                    } else {
                        // Your alternative condition rendering goes here
                        return (<Box component="li" {...props}>
                            <ListItemIcon>
                                <MenuBookIcon/>
                            </ListItemIcon>
                            <ListItemText primary={option.name}/>
                        </Box>);
                    }
                }}
            />
        </div>

        {type === 'product' && <>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Выберите факторы обработки</Text>
                {value && value.factor_ids?.length > 0 ? <div style={{
                    display: 'grid', gridTemplateColumns: 'repeat(3, minmax(0, 1fr))', gap: '12px',
                }}>
                    {factors.filter((factor)=> value.factor_ids.includes(factor.factor_id)).map((factor) => (<div key={factor.factor_id} style={{
                        height: '40x',
                        padding: '4px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        border: '1px solid #D8E0F0',
                        borderRadius: '10px',
                        boxShadow: '0px 2px 2px rgba(0, 0, 0, 0.05)',
                        textAlign: 'center',
                        backgroundColor: productFactor.includes(factor.factor_id) ? '#99E1A4' : productFactor.length === 2 ? "#DFDFDF" : 'white',
                        cursor: productFactor.includes(factor.factor_id) ? 'pointer' : productFactor.length === 2 ? "not-allowed" : 'pointer',
                    }} onClick={() => handleFactorChange(factor.factor_id)}>{factor.name}</div>))}
                </div> : <div style={{
                    height: '40px',
                    padding: '4px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    border: '1px solid #D8E0F0',
                    borderRadius: '10px',
                    boxShadow: '0px 2px 2px rgba(0, 0, 0, 0.05)',
                    textAlign: 'center',
                    backgroundColor: "#DFDFDF",
                }}>{'Факторы не предусмотрены для этого продукта.'}</div>}
            </div>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Укажите БРУТТО вес продукта</Text>
                <NumericFormat
                    size={'small'}
                    value={weight}
                    customInput={TextField}
                    onValueChange={(values) => {
                        // Propagate changes back to the form state
                        setWeight(values.value);
                    }}
                    allowedDecimalSeparators={['%']}
                    fullWidth
                    inputProps={{
                        name: 'weight',
                    }}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">грамм</InputAdornment>,
                    }}
                />
            </div>
        </>}
    </Modal>)
}