import {CustomButton} from "../../../../ui/button/button.jsx";
import Typography from "antd/es/typography";
import {Modal, Table} from "antd";
import {ResultsList} from "../resultsList.jsx";
import {CustomNoRowsOverlay} from "../../../../components/emptyComponent/emptyComponent.jsx";
import {CookingMethodIcons} from "../utils.jsx";

const {Text, Title} = Typography;

export const DetailsModal = ({
                                 open, onCancel, results, standards, type, selectedForDetails, products,
                             }) => {
    const ProductsTableColumns = [{
        title: 'Продукт',
        dataIndex: 'name',
        key: 'name',
        render: (text, record, index) => <span>{`${index + 1}. ${text}`}</span>,
    },
        {
            title: 'Факторы',
            dataIndex: 'pivot',
            key: 'factor_ids',
            render: (ids) => CookingMethodIcons({methods: ids.factor_ids}),
        },
        {
            title: 'Вес', key: 'weight', dataIndex: 'pivot', render: (text) => <span>{text.weight} г</span>,
        }];

    return (<Modal title={<Title
        level={4}>{type === 'product' ? 'Детали по ' : 'Детали по блюду '}<span
        style={{fontStyle: 'italic'}}>{`"${selectedForDetails}"`}</span></Title>}
                   open={open}
                   width={type === 'product' ? 600 : 1000}
                   onCancel={onCancel}
                   footer={[<CustomButton key="delete" onClick={onCancel} type="primary" text={'Закрыть'}
                                          style={{width: '100%'}}/>]}>

        {type === 'product' ?
            <ResultsList result={results} standards={standards}/> :
            <div style={{display: 'flex', gap: '24px'}}>
                <div style={{display: 'flex', flexDirection: 'column', width: '50%'}}>
                    <ResultsList result={results} standards={standards}/>
                </div>
                <div style={{display: 'flex', flexDirection: 'column', width: '50%'}}>
                    <Title level={5} style={{marginBottom: '12px'}}>{'Состав продуктов:'}</Title>
                    <div style={{
                        boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                        borderRadius: '10px',
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '24px'
                    }}>
                        <Table columns={ProductsTableColumns} dataSource={products} pagination={{
                            position: ['none', 'none'],
                        }}
                               size={'small'}
                               locale={{
                                   emptyText: <CustomNoRowsOverlay text={'Добавьте продукты для расчета'}/>,
                               }}
                        /></div>
                </div>

            </div>

        }


    </Modal>)
}