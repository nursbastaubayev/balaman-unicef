import {Modal, Typography} from "antd";
import {CustomButton} from "../../../../ui/button/button.jsx";
import TextField from "@mui/material/TextField";
import {NumericFormat} from "react-number-format";
import InputAdornment from "@mui/material/InputAdornment";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import AcUnitIcon from "@mui/icons-material/AcUnit.js";
import LocalFireDepartmentIcon from "@mui/icons-material/LocalFireDepartment.js";
import MicrowaveIcon from "@mui/icons-material/Microwave.js";
import WaterDropIcon from "@mui/icons-material/WaterDrop.js";
import HotTubIcon from "@mui/icons-material/HotTub.js";
import HeatPumpIcon from "@mui/icons-material/HeatPump.js";
import WhatshotIcon from "@mui/icons-material/Whatshot.js";
import AvTimerIcon from "@mui/icons-material/AvTimer.js";
import OutdoorGrillIcon from "@mui/icons-material/OutdoorGrill.js";

const {Text, Title} = Typography;

export const EditProductModal = ({
                                     open,
                                     onCancel,
                                     onSubmit,
                                     currentEditProduct,
                                     handleEditChange,
                                     setCurrentEditProduct
                                 }) => {

    const iconMapping = {
        1: {icon: <AcUnitIcon/>, title: 'Холодная обработка'},
        2: {icon: <LocalFireDepartmentIcon/>, title: 'Жарка на сковороде с небольшим количеством масла'},
        3: {icon: <MicrowaveIcon/>, title: 'Тушение'},
        4: {icon: <WaterDropIcon/>, title: 'Приготовление на пару'},
        5: {icon: <HotTubIcon/>, title: 'Варка'},
        6: {icon: <HeatPumpIcon/>, title: 'Запекание/выпечка'},
        7: {icon: <WhatshotIcon/>, title: 'Жарка на сухой сковороде'},
        8: {icon: <AvTimerIcon/>, title: 'Медленная варка'},
        9: {icon: <OutdoorGrillIcon/>, title: 'Бланширование'}, // Continue mapping as necessary
    };
    const toggleFactorId = (factorId) => {
        const newFactorIds = currentEditProduct.factor_ids.includes(factorId) ? currentEditProduct.factor_ids.filter(id => id !== factorId) // Remove the factor ID
            : [...currentEditProduct.factor_ids, factorId]; // Add the factor ID

        setCurrentEditProduct({...currentEditProduct, factor_ids: newFactorIds});
    };
    const DialogCookingMethodIcons = ({availableFactors, selectedFactors, toggleFactorId}) => {

        return (<div style={{display: 'flex', flexWrap: 'wrap', gap: '10px'}}>
            {availableFactors.map((factorId) => (<Tooltip key={factorId} title={iconMapping[factorId].title || ''}>
                <IconButton
                    onClick={() => toggleFactorId(factorId)}
                    color={selectedFactors.includes(factorId) ? 'success' : 'default'}
                >
                    {iconMapping[factorId].icon}
                </IconButton>
            </Tooltip>))}
        </div>);
    };

    return (
        <Modal title={<Title level={4}>Редактирование продукта</Title>} open={open} onCancel={onCancel}
               footer={[<CustomButton size={'large'} type={'primary'} onClick={onSubmit} text={'Сохранить'}
                                      style={{width: '100%'}}/>]}>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Название продукта</Text>
                <TextField
                    size={'small'}
                    InputProps={{
                        readOnly: true,
                    }}
                    fullWidth
                    value={currentEditProduct?.product_name}
                />
            </div>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Укажите БРУТТО вес продукта</Text>
                <NumericFormat
                    size={'small'}
                    value={currentEditProduct?.weight}
                    customInput={TextField}
                    onValueChange={(values) => {
                        // Propagate changes back to the form state
                        handleEditChange({
                            target: {
                                name: 'weight', value: values.value,
                            }
                        });
                    }}
                    allowedDecimalSeparators={['%']}
                    fullWidth
                    inputProps={{
                        name: 'weight',
                    }}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">грамм</InputAdornment>,
                    }}
                />
            </div>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Факторы обработки:</Text>
                <DialogCookingMethodIcons
                    availableFactors={currentEditProduct?.availableFactors || []}
                    selectedFactors={currentEditProduct?.factor_ids || []}
                    toggleFactorId={toggleFactorId}
                />
            </div>
        </Modal>
    )
}