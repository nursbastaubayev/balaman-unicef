import {CustomButton} from "../../../../ui/button/button.jsx";
import {Modal} from "antd";
import Typography from "antd/es/typography";
import TextField from "@mui/material/TextField";

const {Text, Title} = Typography;

export const CompileDishModal = ({open, onCancel, onSubmit, createData, handleCreateInputChange}) => {


    return (
        <Modal title={<Title level={4}>Добавления блюда</Title>} open={open}
               onCancel={onCancel}
               footer={[<div style={{display: 'flex', gap: '12px'}}>
                   <CustomButton size={'large'} type={'primary'} onClick={onSubmit} text={'Добавить'}
                                 style={{width: '100%'}} disabled={!createData.name}/>,
                   <CustomButton size={'large'} type={'default'} onClick={onCancel} text={'Отменить'}
                                 style={{width: '100%'}}/>
               </div>]}>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Название блюда</Text>
                <TextField
                    size={'small'}
                    placeholder={'Введите название блюда'}
                    fullWidth
                    value={createData.name}
                    onChange={(e) => handleCreateInputChange('name', e.target.value)}
                />
            </div>
            <div style={{display: 'flex', flexDirection: 'column', gap: '16px', marginBottom: '16px'}}>
                <Text style={{color: "#5B5B5B"}}>Описание блюда</Text>
                <TextField
                    size={'small'}
                    placeholder={'Введите описание блюда'}
                    fullWidth
                    multiline
                    rows={4}
                    value={createData.description}
                    onChange={(e) => handleCreateInputChange('description', e.target.value)}
                />
            </div>
        </Modal>
    )
}