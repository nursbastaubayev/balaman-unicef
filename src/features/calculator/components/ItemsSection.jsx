import Tooltip from "@mui/material/Tooltip";
// import EditIcon from '@mui/icons-material/Edit';
import {useState} from "react";
import {createDish, getDishDetailsByID} from "../../../store/slices/directories/dishesSlice.js";
import {useDispatch} from "react-redux";
import {enqueueSnackbar} from "notistack";
import {calculateMeal} from "../../../store/slices/main/calculatorSlice.js";
import TechCardDownload from "./techCardDownload.jsx";
import {Table} from "antd";
import {CookingMethodIcons} from "./utils.jsx";
import {CustomButton} from "../../../ui/button/button.jsx";
import {DeleteTwoTone, EditTwoTone, InfoCircleTwoTone} from "@ant-design/icons";
import {CustomNoRowsOverlay} from "../../../components/emptyComponent/emptyComponent.jsx";
import {EditProductModal} from "./modals/editProduct.jsx";
import {DetailsModal} from "./modals/details.jsx";
import {CompileDishModal} from "./modals/compileDish.jsx";

export const ItemsSection = ({
                                 addItemDish,
                                 clearProducts,
                                 products = [],
                                 dishes = [],
                                 removeItem,
                                 setProducts,
                                 standards,
                                 demoView
                             }) => {
    const [editModalOpen, setEditModalOpen] = useState(false);
    const [detailsModalOpen, setDetailsModalOpen] = useState(false);
    const [currentEditProduct, setCurrentEditProduct] = useState(null);
    const dispatch = useDispatch()
    const [productDetails, setProductDetails] = useState(null)
    const [createDishModalOpen, setCreateDishModalOpen] = useState(false)

    const ProductsTableColumns = [{
        title: 'Продукт',
        dataIndex: 'product_name',
        key: 'product_name',
        render: (text, record, index) => <span>{`${index + 1}. ${text}`}</span>,
    }, {
        title: 'Факторы',
        dataIndex: 'factor_ids',
        key: 'factor_ids',
        render: (ids) => CookingMethodIcons({methods: ids}),
    }, {
        title: 'Вес', key: 'weight', dataIndex: 'weight', render: (text) => <span>{text} г</span>,
    }, {
        title: '',
        key: 'action',
        render: (_, record, index) => <div style={{display: 'flex', gap: '12px', justifyContent: 'end'}}>
            <Tooltip title={'Редактировать элемент'}>
                <EditTwoTone style={{fontSize: '24px', cursor: 'pointer'}}
                             onClick={() => handleEditClick(record, index)}/>
            </Tooltip>
            <Tooltip title={'Подробная информация'}>
                <InfoCircleTwoTone style={{fontSize: '24px', cursor: 'pointer'}}
                                   onClick={() => getDetails(record, 'product')}/>
            </Tooltip>
            <Tooltip title={'Убрать элемент'}>
                <DeleteTwoTone twoToneColor={'#FF6760'} style={{fontSize: '24px', cursor: 'pointer'}}
                               onClick={() => removeItem(index, 'product')}/>
            </Tooltip>
        </div>
    },];

    const DishesTableColumns = [{
        title: 'Блюдо',
        dataIndex: 'dish_name',
        key: 'dish_name',
        render: (text, record, index) => <span>{`${index + 1}. ${text}`}</span>,
    }, {
        title: '',
        key: 'action',
        render: (_, record, index) => <div style={{display: 'flex', gap: '12px', justifyContent: 'end'}}>
            <Tooltip title={'Подробная информация'}>
                <InfoCircleTwoTone style={{fontSize: '24px', cursor: 'pointer'}}
                                   onClick={() => getDetails(record, 'dish')}/>
            </Tooltip>
            <Tooltip title={'Убрать элемент'}>
                <DeleteTwoTone twoToneColor={'#FF6760'} style={{fontSize: '24px', cursor: 'pointer'}}
                               onClick={() => removeItem(index, 'dish')}/>
            </Tooltip>
        </div>
    },];
    //DETAILS LOGIC

    const [selectedForDetails, setSelectedForDetails] = useState(null)
    const [dishProducts, setDishProducts] = useState([])
    const [type, setType] = useState(null)
    const getDetails = (item, type) => {
        if (type === 'product') {
            setType('product')
            setSelectedForDetails(item.product_name)
            dispatch(calculateMeal({
                products: [item], dishes: [],
            })).then((res) => {
                setProductDetails(res.payload)
                setDetailsModalOpen(true)
            })
        } else {
            setType('dish')
            setSelectedForDetails(item.dish_name)
            dispatch(getDishDetailsByID(item.dish_id)).then((res) => {
                setDishProducts(res.payload.products)
            })
            dispatch(calculateMeal({
                dishes: [item.dish_id], products: [],
            })).then((res) => {
                setProductDetails(res.payload)
                setDetailsModalOpen(true)
            })
        }

    }

    const handleCloseCreateDishModal = () => {
        setCreateDishModalOpen(false)
        setCreateData({name: '', description: '', bls_code: ''})
    }
    const handleCloseEditModal = () => {
        setEditModalOpen(false)
        setCurrentEditProduct(null)
    }
    const handleCloseDetailsModal = () => {
        setSelectedForDetails(null)
        setDetailsModalOpen(false)
        setProductDetails(null)
        setDishProducts([])
        setType(null)
    }
    /////////////////////
    const generateRandomCode = (length = 8) => {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let result = '';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result; // Just return the result
    };
    const [createData, setCreateData] = useState({
        name: '', description: '', bls_code: '',
    })

    // Function to open the edit modal with the selected product
    const handleEditClick = (product, index) => {
        setCurrentEditProduct({...product, index, availableFactors: product.available_factors});
        setEditModalOpen(true);
    };

    const handleCreateInputChange = (key, value) => {
        setCreateData({...createData, [key]: value})
    }
    const handleCreate = () => {
        dispatch(createDish({
            ...createData, products: products, dish_category_id: 1, bls_code: generateRandomCode()
        })).then((res) => {
            clearProducts()
            addItemDish({dish_id: res.payload.dish_id, dish_name: res.payload.name})
            handleCloseCreateDishModal()
            enqueueSnackbar('Блюдо успешно добавлено! Проверьте справочник блюд.', {variant: 'success'})
        })
    }

    // Function to handle changes in the editable fields within the modal
    const handleEditChange = (event) => {
        setCurrentEditProduct({...currentEditProduct, [event.target.name]: event.target.value});
    };
// Function to toggle factor IDs


    // Function to save the edited product back to the products array
    const saveEdit = () => {
        // Clone the current products array
        const updatedProducts = [...products];

        // Update the specific product at the index with new weight and factor_ids
        updatedProducts[currentEditProduct.index] = {
            ...updatedProducts[currentEditProduct.index],
            weight: currentEditProduct.weight,
            factor_ids: currentEditProduct.factor_ids, // Include the updated factor_ids
        };

        // Assuming setProducts is your state setter for the products array
        setProducts(updatedProducts); // Update the state with the modified products array

        setEditModalOpen(false); // Close the edit modal
    };


// Function to render icons for editing, including click handling for selection


    return (<>
        <div style={{
            boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
            borderRadius: '10px',
            display: 'flex',
            flexDirection: 'column',
            gap: '24px'
        }}>
            <Table columns={ProductsTableColumns} dataSource={products} pagination={{
                position: ['none', 'none'],
            }}
                   size={'small'}
                   locale={{
                       emptyText: <CustomNoRowsOverlay text={'Добавьте продукты для расчета'}/>,
                   }}
                   style={{minHeight: '250px'}}
            />
            {!demoView &&
                <div style={{display: 'flex', justifyContent: 'end', padding: '12px', gap: '12px'}}>
                    <CustomButton type={'primary'} onClick={() => setCreateDishModalOpen(true)}
                                  text={'Собрать в блюдо'}/>
                    <TechCardDownload/>
                </div>
            }
        </div>
        <div style={{boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)', borderRadius: '10px'}}>
            <Table columns={DishesTableColumns} dataSource={dishes} pagination={{
                position: ['none', 'none'],
            }} size={'small'}
                   locale={{
                       emptyText: <CustomNoRowsOverlay text={'Добавьте блюда для расчета'}/>,
                   }}
                   style={{minHeight: '250px'}}
            />
        </div>
        <EditProductModal setCurrentEditProduct={setCurrentEditProduct} open={editModalOpen}
                          onCancel={handleCloseEditModal} onSubmit={saveEdit} currentEditProduct={currentEditProduct}
                          handleEditChange={handleEditChange}/>

        <DetailsModal open={detailsModalOpen} onCancel={handleCloseDetailsModal} results={productDetails}
                      standards={standards} selectedForDetails={selectedForDetails} products={dishProducts}
                      type={type}/>

        <CompileDishModal onCancel={handleCloseCreateDishModal} open={createDishModalOpen} onSubmit={handleCreate}
                          createData={createData} handleCreateInputChange={handleCreateInputChange}/>
    </>)
}
