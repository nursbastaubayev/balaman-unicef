import Typography from "@mui/material/Typography";
import {FormControl, Select} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import {ResultsList} from "./resultsList.jsx";
import {consumeStandard} from "./constants.jsx";

export const ResultsSection = ({result, ageCategory, setAgeCategory, standards}) => {
    const handleAgeCategoryChange = (event) => {
        setAgeCategory(event.target.value);
    };

    return (<>
        <div>
            <div style={{display: 'flex', justifyContent: 'end', alignItems: 'center', gap: '12px', margin: '4px'}}>
                <Typography variant={'h8'}>{'Стандарты по возрастам'}</Typography>
                <FormControl>
                    <Select
                        size={'small'}
                        value={ageCategory}
                        onChange={handleAgeCategoryChange}
                        sx={{borderRadius: 0}}
                    >
                        {Object.keys(consumeStandard).map((category) => (
                            <MenuItem key={category} value={category}>{category}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
            <ResultsList result={result} standards={standards}/>
        </div>
    </>)
        ;
}
