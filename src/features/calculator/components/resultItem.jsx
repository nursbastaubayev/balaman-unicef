
import Typograpy from "antd/es/typography";

const {Text, Title} = Typograpy;

const translates = {
    "total_price": 'Цена',
    "total_weight": 'Вес',
    "total_kilocalories": 'Ккал',
    "total_kilocalories_with_fiber": 'Ккал с учетом клетчатки',
    "total_protein": 'Белки',
    "total_fat": 'Жиры',
    "total_carbohydrate": 'Углеводы',
    "water": 'Вода',
    "vitaminA": 'Витамин A',
    "vitaminD": 'Витамин D',
    "vitaminE": 'Витамин E',
    "vitaminK": 'Витамин K',
    "vitaminB1": 'Витамин B1',
    "vitaminB2": 'Витамин B2',
    "vitaminB3": 'Витамин B3',
    "vitaminB5": 'Витамин B5',
    "vitaminB6": 'Витамин B6',
    "vitaminB7": 'Витамин B7',
    "vitaminB9": 'Витамин B9',
    "vitaminB12": 'Витамин B12',
    "vitaminC": 'Витамин C',
    "potassium": 'Калий',
    "calcium": 'Кальций',
    "magnesium": 'Магний',
    "phosphorus": 'Фосфор',
    "iron": 'Железо',
    "zinc": 'Цинк',
    "copper": 'Медь',
    "iodine": 'Йод',
    "sodium": 'Натрий',
    "fiber": 'Пищевые волокна',
}
const PercentageIndicatorItem = ({keyName, value, standard, measurementUnit}) => {
    // Ensure the percentage value is within a valid range (0 to 100)

    function getColor(value, standard) {
        const percentage = (value / standard) * 100;

        if (percentage < 90) {
            return '#FDC748';
        } else if (percentage <= 110) {
            return '#0AC947';
        } else {
            return '#F1463B';
        }
    }

    return (<div style={{display: 'flex', flexDirection: 'column'}}>
            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <Text>{translates[keyName]}</Text>
                <Text
                    style={{color: '#5B5B5B'}}>{standard ? `${value.toFixed(1)} ${measurementUnit} из ${standard.toFixed(1)} ${measurementUnit}` : `${value.toFixed(1)} ${measurementUnit}`}</Text>
            </div>
            <div style={{
                backgroundColor: '#DFDFDF', // Default background color
                color: 'white', display: 'flex', alignItems: 'center', // Center items vertically
                justifyContent: 'space-between', position: 'relative', // Needed for absolute positioning of the background
                borderRadius: '4px', overflow: 'hidden', // Ensures the blue background does not overflow
            }}>
                {/* Blue background     indicating the percentage */}
                <div style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    bottom: 0,
                    width: `${standard ? (value / standard) * 100 : 0}%`, // Adjust width based on the percentage
                    backgroundColor: `${getColor(value, standard)}`,
                    zIndex: 1, // Lower than the text content
                    transition: '0.5s linear',
                }}></div>
                {/* Content to be displayed on top of the blue background */}
                <div style={{
                    position: 'relative',
                    zIndex: 2,
                    display: 'flex',
                    width: '100%',
                    justifyContent: 'space-between',
                    padding: '2px'
                }}>
                    <div style={{
                        width: '100%',
                        padding: '0px 12px',
                        color: 'black',
                        height: '16px',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: "end"
                    }}>
                        <Text>{standard ? `${(value / standard) * 100 >= 0 ? ((value / standard) * 100).toFixed(0) : 0}%` : ''}</Text>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default PercentageIndicatorItem;
