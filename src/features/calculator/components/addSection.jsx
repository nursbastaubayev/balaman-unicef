import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getProductsByNameSearch} from "../../../store/slices/directories/productsSlice.js";
import {getDishesByNameSearch} from "../../../store/slices/directories/dishesSlice.js";
import {getAllFactors} from "../../../store/slices/main/calculatorSlice.js";
import {CustomButton} from "../../../ui/button/button.jsx";
import {Typography} from 'antd';
import {AddProductAndDishModal} from "./modals/addProducts.jsx";

const {Text, Title} = Typography;

export const AddSection = ({addItemProduct, addItemDish, meal_time_number = null}) => {
    const [open, setOpen] = useState({
        boolean: false, type: ''
    });
    const [value, setValue] = useState(null);
    const [weight, setWeight] = useState(100)
    const [inputValue, setInputValue] = useState('');
    const [options, setOptions] = useState([]);
    const factors = useSelector((state) => state.calculator.factors)
    const handleOpen = (type) => setOpen({boolean: true, type: type});
    const handleClose = () => setOpen({boolean: false, type: ''});
    const dispatch = useDispatch();

    useEffect(() => {
        if (open.type === 'product') {
            dispatch(getProductsByNameSearch(inputValue)).then((res) => {
                setOptions(res.payload.data);
            });
        } else {
            dispatch(getDishesByNameSearch(inputValue)).then((res) => {
                setOptions(res.payload.data);
            });
        }
    }, [inputValue, open])

    const [productFactor, setProductFactor] = useState([])

    useEffect(() => {
        setProductFactor([])
    }, [value])
    const handleFactorChange = (newValue) => {
// newValue is number not array, i need to add newValue to productFactor array but also cap it to two elements so no more items will be added

        let updatedFactors = [...productFactor]
        if (updatedFactors.includes(newValue)) {
            updatedFactors = updatedFactors.filter((factor) => factor !== newValue)
        } else {
            if (updatedFactors.length < 2) {
                updatedFactors.push(newValue)
            }
        }
        setProductFactor(updatedFactors)
    };

    useEffect(() => {
        dispatch(getAllFactors())
    }, [])

    const handleSubmit = () => {
        if (open.type === 'product') {
            addItemProduct({
                    product_id: value.product_id,
                    product_name: value.name,
                    factor_ids: productFactor,
                    weight: weight,
                    available_factors: value.factor_ids
                },
                meal_time_number)
        } else {
            addItemDish({
                dish_id: value.dish_id, dish_name: value.name,
            }, meal_time_number)
        }

        handleClose()
        setValue(null)
        setInputValue(null)
        setWeight(100)
        setProductFactor([])
    }

    return (<>
        <div style={{margin: '4px', display: 'flex', gap: '12px'}}>
            <CustomButton type={'primary'} onClick={() => handleOpen('product')}
                          text={'Добавить продукт'}/>
            <CustomButton type={'primary'} onClick={() => handleOpen('dish')} text={'Добавить блюдо'}/>
        </div>
        <Text>
            {'Выберите что хотите добавить в план, после этого внизу отобразятся выбранные продукты и блюда.'}
        </Text>
        <AddProductAndDishModal open={open.boolean} onCancel={handleClose} onSubmit={handleSubmit} type={open.type}
                                options={options} factors={factors} value={value} setValue={setValue}
                                setOptions={setOptions} handleFactorChange={handleFactorChange}
                                weight={weight} setWeight={setWeight} setInputValue={setInputValue}
                                productFactor={productFactor} setProductFactor={setProductFactor}/>

    </>);
}
