import PercentageIndicatorItem from "./resultItem.jsx";
import {Typography} from 'antd';

const {Text, Title} = Typography;

export const ResultsList = ({result, standards}) => {

    const calculatePercentage = (key, value) => {
        const standardValue = standards[key];
        return standardValue ? standardValue : 0;
    };

    return (
        <div style={{display: 'flex', flexDirection: 'column', gap: '12px'}}>
            <div style={{display: 'flex', flexDirection: 'column'}}>
                <Title level={5}>{'Базовая информация:'}</Title>
                <div style={{
                    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                    borderRadius: '10px',
                    display: 'flex',
                    flexDirection: 'column',
                    padding: '12px'
                }}>
                    {result && Object.entries(result.totals).map(([key, value]) => {
                        if (key === 'total_kilocalories') {
                            return PercentageIndicatorItem({
                                keyName: key,
                                value: value,
                                standard: null,
                                measurementUnit: 'kcal',
                            }); // Added missing closing parenthesis here
                        } else if (key === 'total_weight') {
                            return PercentageIndicatorItem({
                                keyName: key,
                                value: value,
                                standard: null,
                                measurementUnit: 'g',
                            }); // Added missing closing parenthesis here
                        } else if (key === 'total_price') {
                            return PercentageIndicatorItem({
                                keyName: key,
                                value: value,
                                standard: value,
                                measurementUnit: '₸',
                            }); // Added missing closing parenthesis here
                        } else {
                            return PercentageIndicatorItem({
                                keyName: key,
                                value: value,
                                standard: calculatePercentage(key, value),
                                measurementUnit: 'g',
                            }); // Added missing closing parenthesis here
                        }
                    })}
                </div>
            </div>

            <div style={{display: 'flex', flexDirection: 'column'}}>
                <Title level={5}>{'Микронутриенты:'}</Title>
                <div style={{
                    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)',
                    borderRadius: '10px',
                    display: 'flex',
                    flexDirection: 'column',
                    padding: '12px'
                }}>
                    <div style={{
                        display: 'grid',
                        gridTemplateColumns: 'repeat(2, minmax(0, 50%))',
                        columnGap: '12px',
                    }}>
                        {result && result.nutrientMap.map((item) =>
                            PercentageIndicatorItem({
                                keyName: item.name,
                                value: item.weight,
                                standard: calculatePercentage(item.name, item.weight),
                                measurementUnit: item.measurement_unit,
                            })
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}
