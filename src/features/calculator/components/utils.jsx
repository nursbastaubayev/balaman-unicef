import Tooltip from "@mui/material/Tooltip";
import AcUnitIcon from "@mui/icons-material/AcUnit.js";
import LocalFireDepartmentIcon from "@mui/icons-material/LocalFireDepartment.js";
import MicrowaveIcon from "@mui/icons-material/Microwave.js";
import WaterDropIcon from "@mui/icons-material/WaterDrop.js";
import HotTubIcon from "@mui/icons-material/HotTub.js";
import HeatPumpIcon from "@mui/icons-material/HeatPump.js";
import WhatshotIcon from "@mui/icons-material/Whatshot.js";
import AvTimerIcon from "@mui/icons-material/AvTimer.js";
import OutdoorGrillIcon from "@mui/icons-material/OutdoorGrill.js";
import FormatColorResetIcon from "@mui/icons-material/FormatColorReset.js";


export const CookingMethodIcons = ({methods}) => {
    const iconMapping = {
        1: <Tooltip title={'Холодная обработка'} key={1}><AcUnitIcon/></Tooltip>,
        2: <Tooltip title={'Жарка на сковороде с небольшим количеством масла'}
                    key={2}><LocalFireDepartmentIcon/></Tooltip>,
        3: <Tooltip title={'Тушение'} key={3}><MicrowaveIcon/></Tooltip>,
        4: <Tooltip title={'Приготовление на пару'} key={4}><WaterDropIcon/></Tooltip>,
        5: <Tooltip title={'Варка'} key={5}><HotTubIcon/></Tooltip>,
        6: <Tooltip title={'Запекание/выпечка'} key={6}><HeatPumpIcon/></Tooltip>,
        7: <Tooltip title={'Жарка на сухой сковороде'} key={7}><WhatshotIcon/></Tooltip>,
        8: <Tooltip title={'Медленная варка'} key={8}><AvTimerIcon/></Tooltip>,
        9: <Tooltip title={'Бланширование'} key={9}><OutdoorGrillIcon/></Tooltip>,
    };

    return (<div>
        {methods.length > 0 ? methods.map((method) => iconMapping[method] || null) :
            <Tooltip title={'Нет дополнительной обработки'} key={10}><FormatColorResetIcon/></Tooltip>}
    </div>);
};