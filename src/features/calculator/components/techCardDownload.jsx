import {instance} from "../../../store/instance.js";

import {CustomButton} from "../../../ui/button/button.jsx";
import React from "react";
class TechCardDownload extends React.Component {
    handleClick = async () => {
        try {
            // Make API request to fetch the Word document data
            const response = await instance.post('/generate-technological-card', {
                products: [
                    {
                        weight: 100,
                        factor_ids: [1],
                        product_id: 11
                    },
                    {
                        weight: 250,
                        factor_ids: [1, 4],
                        product_id: 20
                    }
                ]
            }, );

            // Create a Blob from the response data
            const blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'});

            // Create a URL for the Blob object
            const url = window.URL.createObjectURL(blob);

            // Create a link element dynamically
            const link = document.createElement('a');
            link.href = url;

            // Set the filename for the downloaded file
            link.download = 'document.docx';

            // Programmatically trigger a click on the link to start the download
            document.body.appendChild(link);
            link.click();

            // Clean up by removing the link and revoking the URL
            document.body.removeChild(link);
            window.URL.revokeObjectURL(url);
        } catch (error) {
            console.error('Error downloading document:', error);
        }
    };

    render() {
        return (
            <CustomButton type={'primary'} onClick={this.handleClick}
                          sx={{borderRadius: 0}} text={'Выгрузить тех карту'}/>
        );
    }
}

export default TechCardDownload;

