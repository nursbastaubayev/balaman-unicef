import {AddSection} from "./components/addSection.jsx";
import {ResultsSection} from "./components/resultsSection.jsx";
import {ItemsSection} from "./components/ItemsSection.jsx";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {calculateMeal} from "../../store/slices/main/calculatorSlice.js";
import {consumeStandard} from "./components/constants.jsx";
import {Typography} from 'antd';
import {useLocation} from "react-router-dom";

const {Title} = Typography;
// ----------------------------------------------------------------------
export default function CalculatorMeal() {
    const [products, setProducts] = useState([])
    const [dishes, setDishes] = useState([])
    const dispatch = useDispatch()
    const [result, setResult] = useState(null)
    const [ageCategory, setAgeCategory] = useState('7-10'); // Default to '0-2' age category
    const standards = consumeStandard[ageCategory];
    const addItemProduct = (item) => {
        setProducts([...products, item])
    }
    const removeItem = (index, type) => {
        if (type === 'product') {
            setProducts(products.filter((_, i) => i !== index))
        } else {
            setDishes(dishes.filter((_, i) => i !== index))
        }
    }
    const addItemDish = (item) => {
        setDishes([...dishes, item])
    }

    const clearProducts = () => {
        setProducts([])
    }

    useEffect(() => {
        dispatch(calculateMeal({
            products: products,
            dishes: dishes.map((dish) => dish.dish_id),
        })).then((res) => {
            setResult(res.payload);
        });
    }, [products, dishes]);

    const location = useLocation()

    const demoView = location.pathname === '/calculator-demo'


    return (
        <div style={{display: 'flex', flexDirection: 'column', gap: '24px'}}>
            <div style={{display: 'flex', gap: '24px'}}>
                <div style={{
                    background: '#FFFFFF',
                    width: '60%',
                    padding: '16px',
                    borderRadius: '12px',
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '12px',
                    height: '100%'
                }}>
                    <Title level={4} style={{margin: 0}}>Добавление позиций</Title>
                    <AddSection addItemProduct={addItemProduct} addItemDish={addItemDish}/>
                    <ItemsSection clearProducts={clearProducts} standards={standards} products={products}
                                  dishes={dishes} removeItem={removeItem} setProducts={setProducts}
                                  addItemDish={addItemDish} demoView={demoView}/>
                </div>
                <div style={{background: '#FFFFFF', width: '40%', padding: '16px', borderRadius: '12px'}}>
                    <Title level={4} style={{margin: 0}}>Результаты</Title>
                    <ResultsSection result={result} ageCategory={ageCategory} setAgeCategory={setAgeCategory}
                                    standards={standards}/>
                </div>
            </div>
        </div>
    );
}
